<?php
// required headers
include_once 'config/header.php';

// include database and object files
include_once 'config/database.php';
include_once 'objects/categories.php';

// instantiate database and product object
$database = new Database(require_once "config/config.db.php");
$db = $database->getConnection();

$category = new Categories($db);

// query products
$stmt = $category->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

    // products array
    $category_arr=array();
    $category_arr["records"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $category_item=array(
            "id" => $id,
            'menu'=>$menu
        );

        array_push($category_arr["records"], $category_item);
    }

    echo json_encode($category_arr);
}

else{
    echo json_encode(
        array("message" => "No products found.")
    );
}
?>
