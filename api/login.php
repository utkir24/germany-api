<?php
// required headers
include_once 'config/header.php';

// include database and object files
include_once 'config/database.php';
include_once 'objects/Login.php';
/**
 * @var $request Object
 */
Login::getLogin();
exit();
$config = null;
// instantiate database and product object
$database = new Database(require_once "config/config.db.php");
$db = $database->getConnection();
$login = new Login();
$failed = json_encode(['success' => 0]);
$success = json_encode(['success' => 1]);

if(@$login->validate(@$request_body)) {
    $stmt = @$login->login(@$request_body);
    $num = $stmt->rowCount();
    if ($num > 0) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo $success;
            return ;
        }
    } else {
        echo $failed;
    }
}else {
    echo $failed;
}
?>

