<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$data = file_get_contents("php://input");
if(strlen($data) > 0){
    $request_body = (array)json_decode($data,true);
}else{
    $request_body = array();
}

$request_method = $_SERVER['REQUEST_METHOD'];

if(mb_strtolower($request_method) == mb_strtolower('GET')){
    $request_body = $_GET;
}
$request_object_array = [
    'body' => $request_body,
    'method' => $request_method
];
$methods = array('POST','GET','PUT','DELETE','PATCH','OPTIONS','HEAD');
foreach ($methods as $m){
    $key = 'is'.$m;
    if(mb_strtolower($request_method) == mb_strtolower($m)){
        $request_object_array[$key] = 1;
    }else{
        $request_object_array[$key] = 0;
    }
}
$request = (object)$request_object_array;