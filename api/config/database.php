<?php

class Database
{
// specify your own database credentials
    private $host = "localhost";
    private $db_name = "adm20429_cms2day-v5-fest-des-glaubens-2018";
    private $username = "root";
    private $password = "";
    public $conn;
    public static $connection;

    public function __construct($config = null)
    {
        if(is_array($config)){
            foreach ($config as $c=>$v){
                $this->$c = $v;
            }
        }
    }

    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
        self::$connection = $this->conn;
        return $this->conn;
    }
}