<?php

include_once 'config/header.php';

// include database and object files
include_once 'config/database.php';
include_once 'objects/categories.php';
/**
 * @var $request Object
 */

$config = null;
// instantiate database and product object

$database = new Database(require_once "config/config.db.php");
$db = $database->getConnection();
$category = new Categories($db);

// query category
$stmt = $category->read($request_body);
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){
    $category_arr=array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $category_item = array();
        $category_item += $category->getParams($row);
        $category_item += ['items'=>$category->getItems($id)];
        array_push($category_arr, $category_item);
    }
    echo json_encode($category_arr);
}

else{
    echo json_encode(
        array("message" => "No categories found.")
    );
}
?>
