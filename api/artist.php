<?php
// required headers
include_once 'config/header.php';

// include database and object files
include_once 'config/database.php';
include_once 'objects/Artists.php';
/**
 * @var $request Object
 */
$config = null;
// instantiate database and product object
$database = new Database(require_once "config/config.db.php");
$db = $database->getConnection();
$artist = new Artists();

// query products
$artist = Artists::find([]);
if($artist->count() == 0) {
    echo json_encode(
        array("message" => "No artists found.")
    );
    exit();
}
echo json_encode($artist->all());