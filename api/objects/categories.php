<?php
/**
 * Class Categories
 */
class Categories{
    // database connection and table name
    /**
     * @var
     */
    private $conn;
    /**
     * @var string
     */
    private $table_name = "cm_pages";

    // constructor with $db as database connection

    /**
     * Categories constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->conn = Database::$connection;
        if ($db) {
            $this->conn = $db;
        }
    }

    /**
     * @param $request
     * @return mixed
     */
    public function read($request)
    {
        $query = "SELECT * FROM {$this->table_name } where ";
        if (count($request)) {
            $query .= $this->queryBuild($request);
        } else {
            $query .= ' (submenu=0)';
        }
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    /**
     * @param $id
     * @return array
     */
    public function getItems($id)
    {
        $query = "SELECT * FROM {$this->table_name }  where submenu={$id} ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $num = $stmt->rowCount();
        return $this->getMysqlToArray($num, $stmt);
    }

    /**
     * @param $array
     * @return string
     */
    public function queryBuild($array)
    {
        $new_arr = [];
        foreach ($array as $index => $item) {
            $new_arr [] = '(' . $index . '="' . $item . '")';
        }
        return implode(' and ', $new_arr);
    }

    /**
     * @param $num
     * @param $stmt
     * @return array
     */
    public function getMysqlToArray($num, $stmt)
    {
        if ($num > 0) {
            $category_arr = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $category_item = array();
                array_push($category_item, $this->getParams($row));
                $category_item += ['items' => $this->getItems($id)];
                //array_push($category_item, ['items' => $this->getItems($id)]);
                array_push($category_arr, $category_item);
            }
            return $category_arr;
        }
        return [];
    }

    /**
     * @param $row
     * @return array
     */
    public function getParams($row)
    {
        $array = array();
        foreach ($row as $index => $value) {
            $array[$index] = $value;
        }
        return $array;
    }
}