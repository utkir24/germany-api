<?php
require_once "Model.php";

class Login extends Model
{
    private $table_name = "cm_users";
    public static $headers = [];
    public static $auth_type = "Basic";
    public static $auth_data = [];
    public static $auth_arr = "";
    public static $login = "";
    public static $password = "";

    public function login($filter = [])
    {
        $this->select();
        $this->filter($filter);
        $this->prepare();
        return $this->execute();
    }

    public static function getLogin(){
        self::$headers = apache_request_headers();
        if(isset(self::$headers['Authorization'])){
            self::$auth_data = self::$headers['Authorization'];
        }elseif(isset(self::$headers['authorization'])){
            self::$auth_data = self::$headers['authorization'];
        }else{
            self::$auth_data = null;
        }
        if(preg_match("#".self::$auth_type."#",self::$auth_data)){
            self::$auth_arr = explode(" ",self::$auth_data);
        }

        if(self::$auth_type == "Basic"){
            if(count(self::$auth_data)){
                self::$login = explode(":",base64_decode(self::$auth_arr[1]))[0];
                self::$password = explode(":",base64_decode(self::$auth_arr[1]))[1];
                print_r(self::$password);
                print_r(self::$login);
                exit();
            }
        }
    }


    public function validate($array)
    {
        return array_key_exists('benutzername', $array) && array_key_exists('passwort', $array);
    }

}