<?php

/**
 * Created by PhpStorm.
 * User: jakhar
 * Date: 8/10/18
 * Time: 2:13 AM
 */
class Model
{
    public $query_raw = "";
    public $stmt = null;
    private $isArray = false;
    public function select(){
        $this->query_raw = "SELECT * FROM {$this->tableName() } ";
    }

    public function tableName(){
        return "table_name";
    }

    /**
     * @param array $filter
     * @return null
     */
    public static function find($filter = [],$getStatement = false){
        $object = new static();
        $object->select();
        $object->filter($filter);
        $object->prepare();
        $object->execute();
        if($getStatement){
            return $object->stmt;
        }
        return $object;
    }

    /**
     * @param array $filter
     */
    public function filter($filter = [])
    {
        if(count($filter)){
            $this->andWhere($filter);
        }
    }

    /**
     * @return null
     */
    public function prepare(){
        $this->stmt = Database::$connection->prepare($this->query_raw);
        return $this->stmt;
    }

    /**
     * @return null
     */
    public function execute(){
        $this->stmt->execute();
        return $this->stmt;
    }

    /**
     * @param $array
     * @return string
     */
    public function andWhere($array){
        foreach ($array as $index => $item)
        {
            if(!preg_match("#WHERE#",$this->query_raw)){
                $this->query_raw .= "WHERE ".$index.' LIKE "%'.$item.'%" ';
            }else{
                $this->query_raw .= "AND ".$index.' LIKE "%'.$item.'%" ';
            }
        }
    }

    public function asArray(){
        $this->isArray = true;
    }

    /**
     * @return mixed
     */
    public function all(){
        $data = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!$this->isArray){
            return (object)$data;
        }
        return $data;
    }
    /**
     * @return mixed
     */
    public function one(){
        $data = $this->stmt->fetch(PDO::FETCH_ASSOC);
        if(!$this->isArray){
            return (object)$data;
        }
    }

    /**
     * @return mixed
     */
    public function count(){
        return $this->stmt->rowCount();
    }

    /**
     * @param $table1
     * @param $column1
     * @param $table2
     * @param $column2
     */
    public function join($table1,$column1,$column2){
        $this->query_raw .= " LEFT JOIN $table1 ON ".$table1.".".$column1."=".$this->tableName().".".$column2." ";
    }


}